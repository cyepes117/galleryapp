package com.kogi.galleryapp.ui.listeners;

public interface OnDialogInteractionListener {
    void onPositiveButtonClicked();
    void onNegativeButtonClicked();
}
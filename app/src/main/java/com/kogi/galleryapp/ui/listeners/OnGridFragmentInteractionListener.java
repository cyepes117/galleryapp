package com.kogi.galleryapp.ui.listeners;

public interface OnGridFragmentInteractionListener {
    void onRefreshGrid();
}